'''2022-06-08 11:33:14 test area'''
import logging
from dice.create_dice import create_dice
from dice.roll_dice import roll_dice

# Set up basic configuration for the logging library
logging.basicConfig(filename='roller.log', level=logging.DEBUG,
    format='%(asctime)s|%(levelname)s|%(message)s')

def roller(sides:int=2, iterations:int=5) -> None:
    '''2022-06-11 10:41:35'''
    dice = create_dice(sides)
    roll_sum = 0

    for _ in range(iterations):
        roll = roll_dice(dice)
        roll_sum += roll
        logging.debug(f'Roll: {roll}')

    logging.debug(f'Total = {roll_sum}')

if __name__ == '__main__':
    roller(6, 3)