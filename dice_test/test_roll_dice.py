'''2022-06-08 10:08:48'''
import unittest
import logging
from dice.roll_dice import roll_dice

class TestRollDice(unittest.TestCase):
    '''Multiple handlers and formatters
    Loggers are plain Python objects. The addHandler() method has no minimum or maximum
    quota for the number of handlers you may add. Sometimes it will be beneficial for
    an application to log all messages of all severities to a text file while simultaneously
    logging errors or above to the console. To set this up, simply configure the appropriate
    handlers. The logging calls in the application code will remain unchanged.
    Here is a slight modification to the previous simple module-based configuration example:'''

    # Set up basic configuration for the logging
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    file_handler = logging.FileHandler('test_roll_dice.log')
    file_handler.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    exception_handler = logging.FileHandler('test_roll_dice_exception.log')
    exception_handler.setLevel(logging.ERROR)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s|%(name)s|%(levelname)s|%(message)s')
    exception_handler.setFormatter(formatter)
    file_handler.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(exception_handler)
    logger.addHandler(file_handler)

    @classmethod
    def setUpClass(cls):
        '''2022-06-11 10:57:52 writes logging start on unit_test.log file.'''
        cls.logger.info('Unit test initialized.')


    @classmethod
    def tearDownClass(cls):
        '''2022-06-11 10:57:52 writes logging stop on unit_test.log file.'''
        cls.logger.info('Unit test finished.')


    def test_roll_six_sided_dice(self):
        '''Test 1: Creates a dice of 6 sides and rolls it.
        Assert the roll is in range of 1 to 6.'''
        six_sided_dice = [1, 2, 3, 4, 5, 6]
        roll = roll_dice(six_sided_dice)
        self.assertIn(roll, six_sided_dice)
        self.logger.info('test_roll_six_sided_dice ran successfully.')

    def test_dice_parameter_is_list(self):
        '''Test 2. Validate that dice parameter is actually a list.'''
        six_sided_dice = (1, 2, 3, 4, 5, 6)
        self.assertRaises(TypeError, roll_dice, dice=six_sided_dice)
        self.logger.info('test_dice_parameter_is_list ran successfully.')

    def test_return_data_type(self):
        '''Test 3. Validate result data type in [int, float, complex, bool, str]'''
        self.assertRaises(TypeError, roll_dice, dice=[[1, 2, 3],[4, 5, 6]])
        self.assertRaises(TypeError, roll_dice, dice=[(1, 2, 3),(4, 5, 6)])
        self.assertRaises(TypeError, roll_dice, dice=[range(6), range(6)])
        self.assertRaises(TypeError, roll_dice, dice=[{'key1': 1}, {'key2': 2}])
        self.assertRaises(TypeError, roll_dice, dice=[{"a", "b", "c"}, {1, 2, 3}])
        self.assertRaises(TypeError, roll_dice, dice=[frozenset({1, 2, 3}), frozenset({4, 5, 6})])
        self.assertRaises(TypeError, roll_dice, dice=[b"Hello",b"Bye"])
        self.assertRaises(TypeError, roll_dice, dice=[bytearray(1), bytearray(2)])
        self.assertRaises(TypeError, roll_dice, dice=[memoryview(bytes(5)), memoryview(bytes(4))])
        
        self.logger.info('test_return_data_type ran successfully.')

if __name__ == '__main__':
    unittest.main()