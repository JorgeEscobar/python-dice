'''2022-06-07 22:28:46 unit test file'''
import unittest
import logging
from dice.create_dice import create_dice

class TestCreateDice(unittest.TestCase):
    '''Multiple handlers and formatters
    Loggers are plain Python objects. The addHandler() method has no minimum or maximum
    quota for the number of handlers you may add. Sometimes it will be beneficial for
    an application to log all messages of all severities to a text file while simultaneously
    logging errors or above to the console. To set this up, simply configure the appropriate
    handlers. The logging calls in the application code will remain unchanged.
    Here is a slight modification to the previous simple module-based configuration example:'''

    # Set up basic configuration for the logging
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    file_handler = logging.FileHandler('test_create_dice.log')
    file_handler.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    exception_handler = logging.FileHandler('test_create_dice_exception.log')
    exception_handler.setLevel(logging.ERROR)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s|%(name)s|%(levelname)s|%(message)s')
    exception_handler.setFormatter(formatter)
    file_handler.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(exception_handler)
    logger.addHandler(file_handler)

    @classmethod
    def setUpClass(cls):
        '''2022-06-11 10:57:52 writes logging start on unit_test.log file.'''
        cls.logger.info('Unit test initialized.')


    @classmethod
    def tearDownClass(cls):
        '''2022-06-11 10:57:52 writes logging stop on unit_test.log file.'''
        cls.logger.info('Unit test finished.')



    def test_create_six_sided_dice(self):
        '''Test 1: Creates a dice of 6 sides.
        Returns this list: [1, 2, 3, 4, 5, 6]'''
        expected_result = [1, 2, 3, 4, 5, 6]
        six_sided_dice = create_dice(6)
        self.assertListEqual(expected_result, six_sided_dice)
        self.logger.info('test_create_six_sided_dice ran successfully.')

    def test_create_four_sided_dice(self):
        '''Test 2: Creates a dice of 4 sides.
        Returns this list: [1, 2, 3, 4]'''
        expected_result = [1, 2, 3, 4]
        four_sided_dice = create_dice(4)
        self.assertListEqual(expected_result, four_sided_dice)
        self.logger.info('test_create_four_sided_dice ran successfully.')

    def test_create_dice_data_type_error(self):
        '''Test 3: Validates the argument data type to be int on create_dice().'''
        
        with self.assertRaises(TypeError):
            create_dice(6.6)
            create_dice('two')
            create_dice(5j)
            create_dice(False)

        self.logger.info('test_create_dice_data_type_error ran successfully.')

    def test_create_dice_value_error(self):
        '''Test 4: Validates the argument value to be a possitive int >= 2'''
        
        self.assertRaises(ValueError, create_dice, numbers_of_sides=-2)
        self.assertRaises(ValueError, create_dice, numbers_of_sides=0)
        self.assertRaises(ValueError, create_dice, numbers_of_sides=1)

        self.logger.info('test_create_dice_value_error ran successfully.')

    def test_validate_return_list_data_type(self):
        '''Test 5. Validates that the create_dice function returns a list.'''        
        self.assertIsInstance(create_dice(4), list)

        self.logger.info('test_validate_return_list_data_type ran successfully.')


if __name__ == '__main__':
    unittest.main()