'''2022-06-08 10:07:04 This is where tthe dice rolls.'''
import random

def roll_dice(dice:list):
    '''2022-06-08 10:22:39
    This function returns a random int number from a given list.'''

    # Validate that dice parameter is actually a list
    if type(dice) is not list:
        raise TypeError(f'The \'dice\' parameter must be a list data type. It\'s current data type is {type(dice)}')

    result = random.choice(dice)

    # Validate result data type in [int, float, complex, bool, str]
    if type(result) not in [int, float, complex, bool, str]:
        raise TypeError(f'The \'result\' of the roll must be in [int, float, complex, bool, str] data type. It\'s current data type is {type(result)}')

    return result

if __name__ == '__main__':
    print('Running roll_dice.py')