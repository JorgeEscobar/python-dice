'''2022-06-07 21:41:32 This is a roll dice app in Python'''
  
# create a dice with x sides
def create_dice(numbers_of_sides: int = 2) -> list:
    # Data type validation
    if type(numbers_of_sides) is not int:
        raise TypeError(f'number_of_sides must be an int data type. It\'s current data type is {type(numbers_of_sides)}')

    # Value validation
    if numbers_of_sides < 2:
        raise ValueError(f'Number of sides = {numbers_of_sides} is invalid. Must be at least 2 or more.')

    dice = [_ for _ in range(1, numbers_of_sides+1)]

    # Validate type of dice
    if type(dice) is not list:
        raise TypeError(f'The \'dice\' data type is not a list. It\'s current data type is {type(dice)}')
    
    return dice


if __name__ == '__main__':
    print('Running create_dice.py')